package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/scott.clarke/openqa-worker/v2/pkg/worker"
)

func main() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT)
	c := make(chan string)
	done := make(chan bool, 1)
	if len(os.Args) < 2 {
		log.Fatalf("No WebUI URL supplied")
	}
	w := worker.New()
	err := w.Register(os.Args[1], c, sigs, done)
	if err != nil {
		log.Fatalf("Error in worker register: %v", err)
	}
	fmt.Printf("Worker ID: %d\n", w.ID())
	for {
		select {
		case msg := <-c:
			log.Printf("Got closed: %v", msg)
			err := w.Register(os.Args[1], c, sigs, done)
			if err != nil {
				log.Fatalf("Error in worker register: %v", err)
			}
		case <-done:
			os.Exit(0)
		}
	}
}
