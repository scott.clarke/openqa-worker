package utils

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

func OptimiseImage(path string) error {
	return nil
}

func AddHeaders(req *http.Request, webuiURL *url.URL, apiKey, apiSecret string) {
	req.Header.Set("X-API-Key", apiKey)
	req.Header.Set("X-API-Microtime", strconv.FormatInt(time.Now().Unix(), 10))
	h := hmac.New(sha1.New, []byte(apiSecret))
	h.Write([]byte(webuiURL.RequestURI() + req.Header.Get("X-API-Microtime")))
	req.Header.Add("X-API-Hash", hex.EncodeToString(h.Sum(nil)))
}
