package worker

type autoinstStatus struct {
	CurrentTest         string `json:"current_test,omitempty"`
	Status              string `json:"status,omitempty"`
	TestExecutionPaused int    `json:"test_execution_paused,omitempty"`
}
