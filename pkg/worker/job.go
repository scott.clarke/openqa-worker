package worker

import (
	"encoding/json"
	"os"
	"os/exec"
)

type job struct {
	id        int64
	variables map[string]interface{}
}

func (j *job) start() (*exec.Cmd, error) {
	file, err := os.Create("vars.json")
	if err != nil {
		return nil, err
	}
	if err = json.NewEncoder(file).Encode(j.variables); err != nil {
		return nil, err
	}
	file.Close()
	outfile, err := os.Create("autoinst-log.txt")
	if err != nil {
		return nil, err
	}
	cmd := exec.Command("perl", "/usr/bin/isotovideo", "-d")
	cmd.Stdout = outfile
	cmd.Stderr = outfile
	if err = cmd.Start(); err != nil {
		return nil, err
	}
	return cmd, nil
}
