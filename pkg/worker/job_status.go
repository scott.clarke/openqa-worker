package worker

type jobStatus struct {
	WorkerID  int64              `json:"worker_id,omitempty"`
	Log       *jobLog            `json:"log,omitempty"`
	TestOrder []*testOrder       `json:"test_order,omitempty"`
	Result    map[string]*result `json:"result,omitempty"`
	Uploading int                `json:"uploading,omitempty"`
}

type jobLog struct {
	Data   []byte `json:"data,omitempty"`
	Offset int64  `json:"offset,omitempty"`
}
