package worker

type needle struct {
	Area     []*area `json:"area,omitempty"`
	Error    float64 `json:"error,omitempty"`
	Name     string  `json:"name,omitempty"`
	JSONFile string  `json:"json,omitempty"`
}

type area struct {
	X          int64  `json:"x,omitempty"`
	Y          int64  `json:"y,omitempty"`
	Height     int64  `json:"h,omitempty"`
	Width      int64  `json:"w,omitempty"`
	Result     string `json:"result,omitempty"`
	Similarity int64  `json:"similarity,omitempty"`
}
