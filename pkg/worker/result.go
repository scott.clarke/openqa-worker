package worker

import "encoding/json"

type result struct {
	Dents         int        `json:"dents,omitempty"`
	Details       []*details `json:"details,omitempty"`
	ExecutionTime int64      `json:"execution_time,omitempty"`
	Result        string     `json:"result,omitempty"`
}

type details struct {
	Frametime  []json.Number `json:"frametime,omitempty"`
	Needles    []*needle     `json:"needles,omitempty"`
	Result     string        `json:"result,omitempty"`
	Screenshot *screenshot   `json:"screenshot,omitempty"`
	Tags       []string      `json:"tags,omitempty"`
	Text       string        `json:"text,omitempty"`
	Title      string        `json:"title,omitempty"`
}
