package worker

import "encoding/json"

type screenshot struct {
	Name string `json:"name,omitempty"`
	Md5  string `json:"md5,omitempty"`
}

func (s *screenshot) UnmarshalJSON(data []byte) error {
	var str string
	if err := json.Unmarshal(data, &str); err != nil {
		m := make(map[string]string)
		if err = json.Unmarshal(data, &m); err != nil {
			return err
		}
		s.Name = m["name"]
		s.Md5 = m["md5"]
	}
	s.Name = str
	return nil
}
