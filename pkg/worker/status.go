package worker

type status int64

const (
	free status = iota
	working
	broken
)

func (s status) String() string {
	if s == free {
		return "free"
	}
	if s == working {
		return "working"
	}
	return "broken"
}
