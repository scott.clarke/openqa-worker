package worker

type flags struct {
	Fatal int `json:"fatal,omitempty"`
}

type testOrder struct {
	Name     string `json:"name,omitempty"`
	Script   string `json:"script,omitempty"`
	Category string `json:"category,omitempty"`
	Flags    flags  `json:"flags,omitempty"`
}
