package worker

type uploadResponse struct {
	JobResult   string   `json:"job_result,omitempty"`
	KnownFiles  []string `json:"known_files,omitempty"`
	KnownImages []string `json:"known_images,omitempty"`
	Result      int      `json:"result,omitempty"`
}
