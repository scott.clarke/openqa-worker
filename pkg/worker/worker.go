package worker

import (
	"bytes"
	"context"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
	"sync"
	"syscall"
	"time"

	"gitlab.com/scott.clarke/openqa-worker/v2/pkg/utils"
	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
)

type worker struct {
	id                int64
	capabilities      map[string]string
	ws                *websocket.Conn
	currentJob        *job
	currentStatus     status
	mutex             sync.Mutex
	uploadMutex       sync.Mutex
	webuiURL          string
	logFileOffset     int64
	testOrderMTime    time.Time
	testOrder         []*testOrder
	currentTestModule string
	imagesToSend      map[string]string
	filesToSend       map[string]bool
	knownImages       []string
	knownFiles        []string
	apiKey            string
	apiSecret         string
}

func New() *worker {
	w := &worker{
		imagesToSend: make(map[string]string),
		filesToSend:  make(map[string]bool),
	}
	w.capabilities = map[string]string{
		"host":                  "localhost",
		"cpu_arch":              "x86_64",
		"instance":              "1",
		"mem_max":               "15720",
		"worker_class":          "api_test",
		"websocket_api_version": "1",
	}
	return w
}

func (w *worker) Register(webuiURLString string, c chan string, sigs chan os.Signal, doneChan chan<- bool) error {
	w.webuiURL = webuiURLString
	webuiURL, err := url.Parse(w.webuiURL)
	if err != nil {
		return err
	}
	webuiURL.Path = "/api/v1/workers"
	params := webuiURL.Query()
	for k, v := range w.capabilities {
		params.Add(k, v)
	}
	webuiURL.RawQuery = params.Encode()
	req, err := http.NewRequest("POST", webuiURL.String(), nil)
	if err != nil {
		return err
	}
	utils.AddHeaders(req, webuiURL, w.apiKey, w.apiSecret)
	response, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer response.Body.Close()
	j := make(map[string]interface{})
	err = json.NewDecoder(response.Body).Decode(&j)
	if err != nil {
		return err
	}
	w.id = int64(j["id"].(float64))
	w.currentStatus = free

	ctx := context.Background()

	conn, _, err := websocket.Dial(ctx, fmt.Sprintf("ws://%s/api/v1/ws/%d", webuiURL.Host, w.id), nil)
	if err != nil {
		return err
	}
	w.ws = conn
	m := map[string]string{"type": "worker_status", "status": w.currentStatus.String()}
	err = wsjson.Write(ctx, w.ws, m)
	if err != nil {
		return err
	}
	r := make(chan string)
	go readWebsocket(ctx, w.ws, c, r)
	go w.handleWebsocket(ctx, r, sigs, doneChan)
	return nil
}

func (w *worker) readModuleResult(test string) (*result, error) {
	resultFile, err := os.Open(fmt.Sprintf("testresults/result-%s.json", test))
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return nil, nil
		}
		return nil, err
	}
	moduleResultJSON := &result{}
	err = json.NewDecoder(resultFile).Decode(moduleResultJSON)
	if err != nil {
		return nil, err
	}
	if moduleResultJSON.Details == nil {
		return moduleResultJSON, nil
	}
	for _, detail := range moduleResultJSON.Details {
		if detail.Screenshot == nil && detail.Text == "" {
			continue
		}
		if detail.Screenshot != nil {
			md5Hash := md5.New()
			file, err := os.Open(path.Join("testresults", detail.Screenshot.Name))
			if err != nil {
				return nil, err
			}
			io.Copy(md5Hash, file)
			md5Sum := md5Hash.Sum(nil)
			detail.Screenshot.Md5 = hex.EncodeToString(md5Sum)
			w.imagesToSend[detail.Screenshot.Md5] = detail.Screenshot.Name
			continue
		}
		w.filesToSend[detail.Text] = true
	}
	// Add images and files to send
	return moduleResultJSON, nil
}

func (w *worker) readResultsFile(until string) (map[string]*result, error) {
	ret := make(map[string]*result)
	if w.testOrder == nil {
		return ret, nil
	}
	for _, v := range w.testOrder {
		result, err := w.readModuleResult(v.Name)
		if err != nil {
			return nil, err
		}
		ret[v.Name] = result
		if v.Name == until {
			break
		}
	}
	return ret, nil
}

func (w *worker) uploadExtraFiles() {
	for k, v := range w.imagesToSend {
		relPath := path.Join("testresults", v)
		err := utils.OptimiseImage(relPath)
		if err != nil {
			continue
		}
		if err = w.uploadFile(relPath, false, true, false, k); err != nil {
			log.Printf("Unable to upload image %s: %v", relPath, err)
		}
		thumbRelPath := path.Join("testresults/.thumbs", v)
		err = utils.OptimiseImage(thumbRelPath)
		if err != nil {
			continue
		}
		if err = w.uploadFile(thumbRelPath, false, true, true, k); err != nil {
			log.Printf("Unable to upload thumbnail %s: %v", thumbRelPath, err)
		}
	}
	for f := range w.filesToSend {
		if err := w.uploadFile(path.Join("testresults", f), false, false, false, ""); err != nil {
			log.Printf("Unable to upload file %s: %v", path.Join("testresults", f), err)
		}
	}
	w.imagesToSend = make(map[string]string)
	w.filesToSend = make(map[string]bool)
}

func (w *worker) ignoreKnownFiles() {
	for _, imageMD5 := range w.knownImages {
		delete(w.imagesToSend, imageMD5)
	}
	for _, filename := range w.knownFiles {
		delete(w.filesToSend, filename)
	}
}

func (w *worker) uploadDuring(timer bool) {
	if timer {
		if !w.uploadMutex.TryLock() {
			return
		}
	} else {
		w.uploadMutex.Lock()
	}
	defer w.uploadMutex.Unlock()
	logFile, err := os.Open("autoinst-log.txt")
	if err != nil {
		log.Printf("Couldn't open log file: %v", err)
		return
	}
	ret, err := logFile.Seek(w.logFileOffset, 0)
	if ret != w.logFileOffset || err != nil {
		log.Printf("Error while seeking: %v", err)
		return
	}
	buf, err := io.ReadAll(logFile)
	if err != nil {
		log.Printf("Couldn't read from log file: %v", err)
		return
	}
	logFile.Close()
	currentStatus := &jobStatus{
		WorkerID: w.id,
		Log: &jobLog{
			Data:   buf,
			Offset: w.logFileOffset,
		},
	}
	w.logFileOffset += int64(len(buf))

	statusFilePath := "autoinst-status.json"
	testStatus := &autoinstStatus{}
	if _, err := os.Stat(statusFilePath); !errors.Is(err, os.ErrNotExist) {
		statusFile, err := os.Open(statusFilePath)
		if err != nil {
			log.Printf("Can't open status file: %v", err)
		}
		json.NewDecoder(statusFile).Decode(testStatus)
	}
	if testStatus.Status == "running" || testStatus.Status == "finished" {
		testOrderStat, err := os.Stat("testresults/test_order.json")
		if err != nil {
			log.Printf("Can't open test_order.json: %v", err)
		}
		if testOrderStat.ModTime().After(w.testOrderMTime) {
			w.testOrderMTime = testOrderStat.ModTime()
			testOrderJSON := []*testOrder{}
			testOrderFile, err := os.Open("testresults/test_order.json")
			if err != nil {
				log.Printf("Can't open test_order.json: %v", err)
				return
			}
			err = json.NewDecoder(testOrderFile).Decode(&testOrderJSON)
			if err != nil {
				log.Printf("Failed to decode test order: %v", err)
			}
			currentStatus.TestOrder = testOrderJSON
			w.testOrder = testOrderJSON
		}
	}
	until := ""
	if w.currentTestModule != testStatus.CurrentTest {
		until = w.currentTestModule
	}
	w.currentTestModule = testStatus.CurrentTest

	results, err := w.readResultsFile(until)
	if err != nil {
		log.Printf("Failed to read results file: %v", err)
	}
	if w.currentTestModule != "" {
		results[w.currentTestModule] = &result{
			Result: "running",
		}
	}
	currentStatus.Result = results

	webuiURL, err := url.Parse(w.webuiURL)
	if err != nil {
		log.Printf("Error parsing WebUI URL: %v", err)
		return
	}
	webuiURL.Path = fmt.Sprintf("/api/v1/jobs/%d/status", w.currentJob.id)
	wrappedStatus := map[string]*jobStatus{
		"status": currentStatus,
	}
	var b bytes.Buffer
	err = json.NewEncoder(&b).Encode(wrappedStatus)
	if err != nil {
		log.Printf("Error encoding status: %v", err)
		return
	}
	req, err := http.NewRequest("POST", webuiURL.String(), &b)
	if err != nil {
		log.Printf("Error creating status request: %v", err)
		return
	}
	utils.AddHeaders(req, webuiURL, w.apiKey, w.apiSecret)
	response, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Printf("Error performing POST request: %v", err)
		return
	}
	j := &uploadResponse{}
	err = json.NewDecoder(response.Body).Decode(j)
	if err != nil {
		log.Printf("Error decoding response body: %v", err)
		return
	}
	w.knownImages = j.KnownImages
	w.knownFiles = j.KnownFiles
	w.ignoreKnownFiles()
	w.uploadExtraFiles()
}

func (w *worker) runJob() {
	cmd, err := w.currentJob.start()
	if err != nil {
		log.Printf("There was a problem running the job")
		return
	}
	done := make(chan bool, 1)
	go func(done <-chan bool) {
		for {
			select {
			case <-time.After(time.Second):
				w.uploadDuring(true)
			case <-done:
				return
			}
		}
	}(done)
	nonZeroExit := false
	if err := cmd.Wait(); err != nil {
		if _, ok := err.(*exec.ExitError); !ok {
			log.Printf("Error running isotovideo: %v", err)
		}
		nonZeroExit = true
	}
	done <- true
	w.uploadDuring(false)
	w.endJob(nonZeroExit)
}

func (w *worker) cleanup() {
}

func (w *worker) finaliseJob(failed bool) error {
	params := map[string]string{}
	if failed {
		params["result"] = "incomplete"
		params["reason"] = "Non-zero exit status"
	}
	webuiURL, err := url.Parse(w.webuiURL)
	if err != nil {
		log.Printf("Error parsing WebUI URL: %v", err)
	}
	webuiURL.Path = fmt.Sprintf("/api/v1/jobs/%d/set_done", w.currentJob.id)
	query := webuiURL.Query()
	for k, v := range params {
		query.Add(k, v)
	}
	webuiURL.RawQuery = query.Encode()
	req, err := http.NewRequest("POST", webuiURL.String(), nil)
	if err != nil {
		return err
	}
	utils.AddHeaders(req, webuiURL, w.apiKey, w.apiSecret)
	response, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer response.Body.Close()
	w.mutex.Lock()
	w.currentJob = nil
	w.currentStatus = free
	w.testOrder = nil
	w.filesToSend = make(map[string]bool)
	w.imagesToSend = make(map[string]string)
	w.cleanup()
	w.mutex.Unlock()
	return nil
}

func (w *worker) endJob(failed bool) {
	currentStatus := map[string]*jobStatus{
		"status": {
			WorkerID:  w.id,
			Uploading: 1,
		},
	}
	webuiURL, err := url.Parse(w.webuiURL)
	if err != nil {
		log.Printf("Error parsing WebUI URL: %v", err)
	}
	webuiURL.Path = fmt.Sprintf("/api/v1/jobs/%d/status", w.currentJob.id)
	var b bytes.Buffer
	json.NewEncoder(&b).Encode(currentStatus)
	req, err := http.NewRequest("POST", webuiURL.String(), &b)
	if err != nil {
		log.Printf("Error creating status request: %v", err)
		return
	}
	utils.AddHeaders(req, webuiURL, w.apiKey, w.apiSecret)
	response, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Printf("Error performing POST request: %v", err)
	}
	defer response.Body.Close()
	j := make(map[string]interface{})
	err = json.NewDecoder(response.Body).Decode(&j)
	if err != nil {
		log.Printf("Error decoding response body: %v", err)
	}
	w.uploadResults()
	w.finaliseJob(failed)
}

func cleanFilename(filename string) string {
	/*
		$ofile =~ s/serial0/serial0.txt/;
		$ofile =~ s/virtio_console(.*)\.log/serial_terminal$1.txt/;
		$ofile =~ s/\.log/.txt/;
	*/
	if filename == "serial0" {
		return "serial0.txt"
	}
	filename = strings.Replace(filename, "virtio_console", "serial_terminal", 1)
	filename = strings.Replace(filename, ".log", ".txt", 1)
	return filename
}

func (w *worker) uploadFile(filename string, ulogs bool, image, thumb bool, md5 string) error {
	webuiURL, err := url.Parse(w.webuiURL)
	if err != nil {
		return err
	}
	webuiURL.Path = fmt.Sprintf("/api/v1/jobs/%d/artefact", w.currentJob.id)
	var b bytes.Buffer
	formWriter := multipart.NewWriter(&b)
	cleanedFilename := cleanFilename(filename)
	part, err := formWriter.CreateFormFile("file", cleanedFilename)
	if err != nil {
		return err
	}
	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	if _, err = io.Copy(part, file); err != nil {
		return err
	}
	if ulogs {
		formWriter.WriteField("ulogs", "1")
	}
	if image {
		formWriter.WriteField("image", "1")
		formWriter.WriteField("md5", md5)
	}
	if thumb {
		formWriter.WriteField("thumb", "1")
	}
	formWriter.Close()
	req, err := http.NewRequest("POST", webuiURL.String(), &b)
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", formWriter.FormDataContentType())
	utils.AddHeaders(req, webuiURL, w.apiKey, w.apiSecret)
	response, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer response.Body.Close()
	if response.StatusCode < 200 || response.StatusCode > 299 {
		return fmt.Errorf("status code is not successful: %s", response.Status)
	}
	return nil
}

func (w *worker) uploadResults() error {
	// Uploads are posted to jobs/<job_id>/artefact with a form containing a file and a filename
	// Looks like manually opening and reading files may be required, it's
	// possible Go has similar convenience fucntions for this though
	// Upload files in ulogs directory (with ulog = 1)
	matches, err := filepath.Glob("ulogs/*")
	if err != nil {
		return err
	}
	for _, filename := range matches {
		err = w.uploadFile(filename, true, false, false, "")
		if err != nil {
			return err
		}
	}

	// Upload videos
	matches, err = filepath.Glob("*video.*")
	if err != nil {
		return err
	}
	for _, filename := range matches {
		err = w.uploadFile(filename, false, false, false, "")
		if err != nil {
			return err
		}
	}

	// Upload 'vars.json', 'autoinst-log.txt', 'worker-log.txt'
	// Check they exist first probably
	uploadFiles := []string{
		"vars.json",
		"autoinst-log.txt",
		"worker-log.txt",
		"serial0",
		"video_time.vtt",
		"serial_terminal.txt",
		"virtio_console.log",
		"virtio_console1.log",
		"virtio_console_user.log",
	}
	for _, f := range uploadFiles {
		if _, err := os.Stat(f); errors.Is(err, os.ErrNotExist) {
			continue
		}
		err = w.uploadFile(f, false, false, false, "")
		if err != nil {
			return err
		}
	}
	return nil
}

func readWebsocket(ctx context.Context, conn *websocket.Conn, channel chan string, read chan string) {
	for {
		messageType, message, err := conn.Read(ctx)
		if err != nil {
			log.Printf("Error in read: %v", err)
			channel <- "closed"
			close(read)
			return
		}
		if messageType == websocket.MessageText {
			read <- string(message)
		}
	}
}

func (w *worker) handleWebsocket(ctx context.Context, data <-chan string, signals chan os.Signal, done chan<- bool) {
	for {
		select {
		case d, more := <-data:
			if !more {
				return
			}
			jsonData := make(map[string]interface{})
			err := json.NewDecoder(strings.NewReader(d)).Decode(&jsonData)
			if err != nil {
				log.Printf("Got non-JSON message, ignoring: %v", err)
				break
			}
			if jsonData["type"] == "grab_job" {
				w.mutex.Lock()
				w.currentJob = &job{
					id:        int64(jsonData["job"].(map[string]interface{})["id"].(float64)),
					variables: jsonData["job"].(map[string]interface{})["settings"].(map[string]interface{}),
				}
				w.currentStatus = working
				w.mutex.Unlock()
				message := make(map[string]interface{})
				message["type"] = "accepted"
				message["jobid"] = w.currentJob.id
				wsjson.Write(ctx, w.ws, message)
				go w.runJob()
				log.Printf("Job: %v", w.currentJob)
			}
		case s := <-signals:
			if s == syscall.SIGINT {
				log.Printf("Received SIGINT, going offline")
				message := make(map[string]string)
				message["type"] = "quit"
				wsjson.Write(ctx, w.ws, message)
				done <- true
				return
			}
		case <-time.After(time.Second * 10):
			message := make(map[string]interface{})
			message["type"] = "worker_status"
			w.mutex.Lock()
			message["status"] = w.currentStatus.String()
			w.mutex.Unlock()
			wsjson.Write(ctx, w.ws, message)
		}
	}
}

func (w *worker) ID() int64 {
	return w.id
}
